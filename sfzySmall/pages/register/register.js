//Page Object
Page({
    data: {

    },
    backLogin() {
        wx.navigateBack({
            delta: 1
        })
    },
    submitData(e) {
        wx.request({
            url: 'http://localhost:3007/api/reguser',
            method: "post",
            data: e.detail.value,
            header: {
                'content-Type': 'application/x-www-form-urlencoded'
            },
            success: function(res) {
                if (res.data.status) {
                    wx.showToast({
                        title: res.data.message,
                        icon: 'none'
                    })
                } else {
                    wx.navigateTo({
                        url: "/pages/login/login"
                    });
                    wx.showToast({
                        title: res.data.message,
                        icon: 'none'
                    })
                }
            }
        })
    },
    //options(Object)
    onLoad: function(options) {

    },
    onReady: function() {

    },
    onShow: function() {

    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {

    },
    onReachBottom: function() {

    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});