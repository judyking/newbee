//Page Object
Page({
    data: {
        imageUrls: [],
        itemList: []
    },
    //options(Object)
    onLoad: function(options) {
        // lunbotu
        wx.request({
            url: 'https://wwyy.wzhxlx.com/wwyy/course/queryAllCourse?channelId=409&pageNo=1&pageSize=10',
            method: 'GET',
            success: (result) => {
                console.log(result);
                this.setData({ //此时OK
                    imageUrls: result.data.data.list.slice(0, 4),
                    itemList: result.data.data.list.slice(4)
                })
            },
            fail: () => {},
            complete: () => {}
        });
    },
    onReady: function() {

    },
    onShow: function() {

    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {

    },
    onReachBottom: function() {

    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});