//Page Object
Page({
    data: {
        articles: [],
        pageNo: 1,
        totalPageCount: 0,
        isLoading: false
    },
    //请求封装
    getData() {
        this.setData({
            isLoading: true
        })

        wx.showLoading({
            title: "加载中...",
            icon: "loading"
        });

        wx.request({
            url: `https://wwyy.wzhxlx.com/wwyy/article/query?channelId=409&pageNo=${this.data.pageNo}&pageSize=15`,
            method: 'GET',
            success: (result) => {
                console.log(result.data.data.list);
                this.setData({ //此时OK
                    articles: [...this.data.articles, ...result.data.data.list],
                    // articles: result.data.data.list,
                    totalPageCount: result.data.data.totalPageCount,
                    isLoading: false

                })
                console.log(this.data.articles);
            },
            fail: () => {},
            complete: () => {
                wx.hideLoading();
                wx.stopPullDownRefresh();
            }
        });
    },
    //options(Object)
    onLoad: function(options) {
        // atricles
        this.getData()
    },
    onReady: function() {

    },
    onShow: function() {

    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {
        this.setData({
            articles: [],
            pageNo: 1,
            totalPageCount: 0
        })
        this.getData()
    },
    onReachBottom: function() {
        if (this.data.isLoading) return
            //数量加载完毕 不发请求
        if (this.data.pageNo >= this.data.totalPageCount) {
            wx.showToast({
                title: '没有数据了',
                icon: "none"
            })
            return
        }
        this.setData({
            pageNo: this.data.pageNo + 1
        })
        this.getData()
    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});