//Page Object
let app = getApp();

Page({
    data: {
        users: {},
    },
    //options(Object)
    onLoad: function(options) {
        //(app.globalData.loginUserinfo为true就显示我的页面 false重定向到登陆页面
        if (app.globalData.loginUserinfo) {
            this.setData({
                users: app.globalData.loginUserinfo
            })
        } else {
            wx.redirectTo({
                url: '/pages/login/login?backUrl=/pages/my/my'
            })
        }
    },
    exitLoginHandle() {
        app.globalData.loginUserinfo = null
        wx.switchTab({
            url: "/pages/index/index"
        })
    },
    onReady: function() {

    },
    onShow: function() {
        if (app.globalData.loginUserinfo) {
            this.setData({
                users: app.globalData.loginUserinfo
            })
            console.log(this.data.users);
        } else {
            wx.redirectTo({
                url: '/pages/login/login'
            })
        }
    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {

    },
    onReachBottom: function() {

    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});