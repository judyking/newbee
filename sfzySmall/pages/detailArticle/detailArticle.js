var WxParse = require('../../wxParse/wxParse.js');

//Page Object
Page({
    data: {
        id: 0,
        detail: []
    },
    //options(Object)
    onLoad: function(options) {
        console.log(options);
        this.setData({
            id: options.id
        })
        wx.request({
            method: "get",
            url: `https://wwyy.wzhxlx.com/wwyy/article/detail?articleId=13789&channelId=${this.data.id}`,
            header: {
                'Content-Type': 'application/json'
            },
            success: (res) => {
                // console.log(res.data.data);
                let detail = res.data.data
                WxParse.wxParse('detail', 'html', detail.detail, this, 0);
                this.setData({
                    detail: detail
                })
            }
        })
    },
    onReady: function() {

    },
    onShow: function() {

    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {

    },
    onReachBottom: function() {

    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});