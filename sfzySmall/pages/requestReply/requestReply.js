//Page Object
Page({
    data: {
        title: "",
        content: "",
        color: "",
        placeholder: "标题(必填)",
    },
    //输入框内容
    sendOut() {
        if (this.data.title === "") {
            this.setData({
                color: "color:#F76260",
                placeholder: "标题不能为空"
            })
            return
        }
        wx.redirectTo({
            url: `/pages/answering/answering?title=${this.data.title}&content=${this.data.content}`
        })
    },

    //重定向到
    redirect() {
        wx.redirectTo({
            url: '/pages/answering/answering'
        })
    },

    //input 事件 bindinput 避免报错
    fakeCallback() {

    },
    //options(Object)
    onLoad: function(options) {

    },
    onReady: function() {

    },
    onShow: function() {

    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {

    },
    onReachBottom: function() {

    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});