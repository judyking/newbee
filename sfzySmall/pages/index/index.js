const app = getApp();

//Page Object
Page({
    data: {
        imageUrls: [],
        navs: [],
        articles: [],
    },
    goRequestReply() {
        const loginUserinfo = app.globalData.loginUserinfo
        if (loginUserinfo) {
            wx.navigateTo({
                url: '/pages/requestReply/requestReply'
            })
        } else {
            wx.redirectTo({
                url: '/pages/login/login?backUrl=/pages/requestReply/requestReply'
            })
        }
    },
    //options(Object)
    onLoad: function(options) {
        // lunbotu
        wx.request({
            url: 'https://wwyy.wzhxlx.com/wwyy/article/getTopWeekChart?channelId=409&pageNo=1&pageSize=3&topType=1',
            method: 'GET',
            success: (result) => {
                this.setData({ //此时OK
                    imageUrls: result.data.data.list
                })
                console.log(this.data.imageUrls);
            },
            fail: () => {},
            complete: () => {}
        });
        //navs
        wx.request({
            url: 'http://localhost:3004/navs',
            method: 'GET',
            success: (result) => {
                console.log(result);
                this.setData({ //此时OK
                    navs: result.data
                })
            },
            fail: () => {},
            complete: () => {}
        });
        // atriccles
        wx.request({
            url: 'https://wwyy.wzhxlx.com/wwyy/article/query?channelId=409&pageNo=1&pageSize=3',
            method: 'GET',
            success: (result) => {
                console.log(result);
                this.setData({ //此时OK
                    articles: result.data.data.list
                })
            },
            fail: () => {},
            complete: () => {}
        });
    },
    onReady: function() {

    },
    onShow: function() {

    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {

    },
    onReachBottom: function() {

    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});