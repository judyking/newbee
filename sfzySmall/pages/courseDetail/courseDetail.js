//Page Object
Page({
    data: {
        id: 0
    },
    //options(Object)
    onLoad: function(options) {
        console.log(options);
        this.setData({
            id: options.id
        })
    },
    onReady: function() {

    },
    onShow: function() {

    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {

    },
    onReachBottom: function() {

    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});