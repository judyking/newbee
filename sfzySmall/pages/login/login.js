//Page Object
let app = getApp();


Page({
    data: {
        inputUserinfo: {},
        token: "",
        backUrl: ""
    },

    submitHandle(e) {
        //本地缓存账号密码 用于回显
        wx.setStorageSync("inputUserinfo", e.detail.value)

        wx.request({
            method: "post",
            url: 'http://localhost:3007/api/login',
            data: e.detail.value,
            header: {
                'content-Type': 'application/x-www-form-urlencoded'
            },
            success: (res) => {
                const that = this
                this.setData({
                    token: res.data.token
                })
                if (res.data.status) {
                    //res.data.status=0
                    wx.showToast({
                        title: res.data.message,
                        icon: 'none'
                    })
                } else {
                    wx.request({
                        method: "get",
                        url: 'http://localhost:3007/my/userinfo',
                        header: {
                            'Authorization': this.data.token
                        },
                        success: (res) => {
                            console.log(res.data.data);
                            app.globalData.loginUserinfo = {...res.data.data, id: "" }
                            console.log(app.globalData.loginUserinfo);

                            if (that.data.backUrl == "/pages/my/my") {
                                wx.switchTab({
                                    url: that.data.backUrl,
                                });
                            } else if (that.data.backUrl == undefined) {
                                wx.switchTab({
                                    url: "/pages/index/index",
                                });
                            } else {
                                console.log(that.data.backUrl);
                                wx.navigateTo({
                                    url: that.data.backUrl,
                                });
                            }

                        }
                    })
                }
            }
        })
    },
    //options(Object)
    onLoad: function(options) {
        this.setData({
            backUrl: options.backUrl
        })
        console.log(this.data.backUrl);
        //读取缓存
        const inputUserinfo = wx.getStorageSync('inputUserinfo')
        if (inputUserinfo) {
            this.setData({
                inputUserinfo
            })
        }
    },
    onReady: function() {

    },
    onShow: function() {

    },
    onHide: function() {

    },
    onUnload: function() {

    },
    onPullDownRefresh: function() {

    },
    onReachBottom: function() {

    },
    onShareAppMessage: function() {

    },
    onPageScroll: function() {

    },
    //item(index,pagePath,text)
    onTabItemTap: function(item) {

    }
});