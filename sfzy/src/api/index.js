import axios from 'axios';

export function query(request, route, data) {
    console.log(data);
    return axios({
        baseURL: 'http://localhost:3004/',
        method: request,
        url: route,
        data: data
    })
}