import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import User from '../views/user/User.vue'
import Teacher from '../views/Teacher.vue'
import Role from '../views/Role.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        redirect: "/home"
    },
    {
        path: '/home',
        name: 'home',
        component: Home,
        children: [
            { path: '/home', redirect: '/home/user' },
            { path: '/home/user', component: User },
            { path: '/home/teacher', component: Teacher },
            { path: '/home/role', component: Role }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
]

const router = new VueRouter({
    routes
})

// 全局路由守卫
router.beforeEach((to, from, next) => {
    if (to.path == "/login") {
        if (sessionStorage.getItem("username")) {
            // 登录了，去/login
            router.push("/home")
        } else {
            // 没有登录，去/login
            next();
        }
    }
    // 访问不是/login  可能是/home
    if (sessionStorage.getItem("username")) {
        // 登录了 访问/home 
        next()
    } else {
        // 没有登录，访问/home
        router.push("/login")
    }
})

export default router